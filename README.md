# review_process_template

a template to copy for security review of Tezos Smart Contracts

***

## Getting started

- [ ] create a new repository using this repository as a template
- [ ] clone the repository to review, into the main branch
- [ ] commit code comments into the review branch
- [ ] take and share notes within the collaboration file


## Start the review process

- [ ] kickoff
- [ ] checklist
