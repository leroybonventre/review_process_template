🔴 🟠 🟡 🟢	🔵 🟣 🟤 ⚫ ⚪ bigger projects could use country flags

Documentation for:

    - System / Platform documentation
    - Contract origination and initialisation
    - Entrypoints
    - on- and off-chain views


# Entrypoints

## Information about Entrypoint Documentation

- Location of the entrypoint documentation within the code repository
- Location of the entrypoint documentation online

## Checklist per entrypoint (copy and paste the block below for each entrypoint)
Name: 

entrypoint name | documentation | parameters | parameters types | authorization | tez | callbacks | calls to untrusted | operation ordering | processing values | storing values | calculations | testing
---------- | ---------- | ---------- | ---------- | ---------- | ---------- | ---------- | ---------- | ---------- | ---------- | ---------- | ---------- | ----------
🔴 entrypoint_1 | OK | OK | n/a | NOK | Ok | OK | NOK | n/a | OK | OK | Ok | OK
🟠 entrypoint_2 | OK | OK | n/a | NOK | Ok | OK | NOK | n/a | OK | OK | Ok | OK
  |  |  |  |  |  |  |  |  |  |  |  | 


### 1. Entrypoint documentation & specification
Check documentation and specification for this entrypoint. The documentation/specification should at least provide information about:
- Description what the entrypoint is for and what it should do
- Meaning and types of input parameters / arguments
- Pre conditions (e.g.who can call the entrypoint)
- Post conditions
- Returned operations (transfers, contract origination, etc.)
***
🔴 entrypoint_1: good doc

🟠 entrypoint_2: not good doc
***

### 2. Entrypoint parameters ([TSCD-001](https://github.com/InferenceAG/TezosSmartContractDetails/blob/master/items/TSCD-001.md))
Check entrypoint parameters for its purpose and whether the entrypoint and all of its parameters are really required. Also check whether parameters can be used in a way different to the specification of the entrypoint.

Example:
A “from” parameter is not required if the entrypoint is always sending tez from “sender” or “source” address to a destination address.
***

### 3. Entrypoint parameter types 
Check entrypoint parameters whether they have the appropriate type.

Example:
A number which is always positive by design/specification such as e.g. a token amount should be of type NAT and not INT.
***

### 4. Authorization check ([TSCD-004](https://github.com/InferenceAG/TezosSmartContractDetails/blob/master/items/TSCD-004.md), [TSCD-006](https://github.com/InferenceAG/TezosSmartContractDetails/blob/master/items/TSCD-006.md))
Check entrypoint implementation whether an authorization check is implemented and check whether implementation is correctly performing according to the entrypoint’s specification.

Consider:
“sender” vs “source” instruction to obtain the correct address.
***

### 5. Handling of transferred tez ([TSCD-011](https://github.com/InferenceAG/TezosSmartContractDetails/blob/master/items/TSCD-011.md))
Check whether the entrypoint is correctly dealing with tez transferred to and check whether this is in line with the entrypoint’s specification.

Additional note:
In general, the entrypoint should fail, if the entrypoint does not expect any tez. 
If the entrypoint is accepting tez, ensure that the tez is correctly handled by checking e.g. that any balance tracking values are correctly updated.
***

### 6. Callbacks: Information obtained from other contracts ([TSCD-014](https://github.com/InferenceAG/TezosSmartContractDetails/blob/master/items/TSCD-014.md))
Check if entrypoint’s code is correctly handling information obtained from other contracts using callbacks. 

In case the entrypoint is a “setter” entrypoint in a callback scheme, also analyze whether appropriate checks are implemented to ensure only intended contracts within the correct operation flow can call this setter function.

Additional note:
Due to Tezos message passing style the operation is executed, after the entrypoint’s code has been executed. Thus, values called via an operation are not immediately available.

Additional note:
In general, avoid obtaining information from other contracts, if possible. Consider implementing and using “on-chain views”.
***

### 7. Calls to untrusted addresses ([TSCD-009](https://github.com/InferenceAG/TezosSmartContractDetails/blob/master/items/TSCD-009.md), [TSCD-013](https://github.com/InferenceAG/TezosSmartContractDetails/blob/master/items/TSCD-013.md))
In case the entrypoint is crafting operations to untrusted contracts, carefully analyse for any security risk/issues, since the called addresses may not act in the way the smart contract coder assumes.

Potential security risks are:	
Gas exhausting attacks
DoS attacks
Providing wrong answers / values
Reentrancy and call ordering attacks
Replacement of code (using lambda functions stored in big_maps)
Destination contract can make the whole transaction to fail

Consider and investigate:
Contract allows the registration of arbitrary addresses - implicit, but also smart contract addresses. 
Any tez payout functions. Note: Generally it is recommended to implement a solution where users have to claim their payout instead that the contract pays tez out. 
Consider implementing a check that excludes that callee can be a smart contract (SmartPy: “sp.sender <= "tz3jfebmewtfXYD1Xef34TwrfMg2rrrw6oum")

See also the token integration checklist:
https://gitlab.com/camlcase-dev/dexter/-/blob/develop/docs/token-integration.md
***

### 8. Operation ordering ([TSCD-015](https://github.com/InferenceAG/TezosSmartContractDetails/blob/master/items/TSCD-015.md))
Check whether the operations crafted by the entrypoint are in the correct logical order and in line with the entrypoint’s specification.
***

### 9. Processing with wrong values (check in Michelson) 
Check whether the entrypoint is correctly processing values.

Example:
Subsequent code takes an initial storage or entrypoint value instead of an updated value.
Subsequent code takes an already calculated value instead of the original value provided via a storage or an entrypoint parameter.
***

### 10. Storing of wrong values (check in Michelson)
Check whether the entrypoint’s resulting storage has wrong (updated) values in.

Example:
The values in the output storage may not be the final calculated values, but the input values or some intermediary calculated values due to wrong storage handling by the entrypoint’s code. Lookout for Michelson instructions such as: Update, Update n, and Pair. 
***

### 11. Calculations
Include “calculations” test cases in case the entrypoint has any calculations in.
***

### 12. Testing
Check whether the on-chain view is appropriately covered with testing. Consider coverage with unit testing, integration testing, and mutation testing.
***
🔴 entrypoint_1: good testing

🟠 entrypoint_2: not good testing
***
